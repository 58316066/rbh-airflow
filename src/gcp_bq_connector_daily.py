import json
from datetime import datetime

from airflow.decorators import dag, task
from airflow.providers.amazon.aws.operators.glue import AwsGlueJobOperator


@dag(schedule_interval=None, start_date=datetime(2021, 10, 4), catchup=False, tags=['poc'])
def gcp_bq_connector_daily():
    glue_ingestion_task = AwsGlueJobOperator(
        task_id="ingest_gcp_events",
        job_name="airflow_ingest_gcp_events",
        job_desc="Airflow dag gcp_bq_connector_daily",
        region_name='ap-southeast-1',
        num_of_dpus=0,
        script_location='s3://nat-mwaa-de-airflow-poc/dags/gcp_bq_connector/asset/glue_ingest_gcp_events.py',
        iam_role_name='poc-glue-bq-connector',
        aws_conn_id='aws_role_poc_glue_bq_connector_conn',
        s3_bucket='s3://nat-mwaa-de-airflow-poc',
        retry_limit=1,
        create_job_kwargs={
            "Connections": {"Connections": ["gcp-bq-glue-3-conn"]},
            "GlueVersion": "3.0",
            "WorkerType": "G.1X",
            "NumberOfWorkers": 2,
            "DefaultArguments": {
                "--TS_FILTER": "1633322718946500"
            }
        }
    )

    glue_ingestion_task


gcp_bq_connector_daily = gcp_bq_connector_daily()
