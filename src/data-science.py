import json
from datetime import datetime

from airflow.decorators import dag, task
from airflow.providers.amazon.aws.operators.glue import AwsGlueJobOperator

from airflow import DAG
from airflow.utils.dates import days_ago
from datetime import timedelta
import os

DAG_ID = "rider-dispatch-feature-ingestion"
DEFAULT_ARGS = {
    'owner': 'patraporn',
    'depends_on_past': False,
    'email': ['patraporn.kuhavattana@scb.co.th'],
    'email_on_failure': False,
    'email_on_retry': False,
}


with DAG(
    dag_id=DAG_ID,
    default_args=DEFAULT_ARGS,
    start_date=days_ago(1),
	catchup=False,
    schedule_interval='@once',
    tags=['rider-dispatch', 'feature-ingestion'],
) as dag:


	data_validation_task = AwsGlueJobOperator(
        task_id="validate-data",
        job_name="rider-dispatch-data-validation",
        job_desc="Glue job to validate input data for feature ingestion",
        region_name='ap-southeast-1',
        num_of_dpus=0,
        script_location='s3://datasci-poc-dag/dag/asset/rider_dispatch_data_validation.py',
        iam_role_name='AWSGlueServiceRole-robinhood-live',
        s3_bucket='s3://datasci-poc-dag',
        retry_limit=0,
        create_job_kwargs={
            "GlueVersion": "3.0",
            "WorkerType": "G.1X",
            "NumberOfWorkers": 10,
        },
        script_args={
            "--busi_dt" : "{{ dag_run.conf['busi_dt'] }}"
		}
    )
	
	rider_weekly_ingestion_task = AwsGlueJobOperator(
        task_id="ingest-rider-data",
        job_name="rider-weekly-data-ingestion",
        job_desc="Glue job to ingest rider weekly feature into a feature store",
        region_name='ap-southeast-1',
        num_of_dpus=0,
        script_location='s3://datasci-poc-dag/dag/asset/rider_dispatch_rider_data_ingestion.py',
        iam_role_name='AWSGlueServiceRole-robinhood-live',
        s3_bucket='s3://datasci-poc-dag',
        retry_limit=0,
        create_job_kwargs={
            "GlueVersion": "3.0",
            "WorkerType": "G.1X",
            "NumberOfWorkers": 10,
            "DefaultArguments": {
                "--additional-python-modules": "sagemaker"
            }
        },
        script_args={
            "--busi_dt" : "{{ dag_run.conf['busi_dt'] }}"
		}
    )
	
	shop_weekly_ingestion_task = AwsGlueJobOperator(
        task_id="ingest-shop-data",
        job_name="shop-weekly-data-ingestion",
        job_desc="Glue job to ingest shop weekly feature into a feature store",
        region_name='ap-southeast-1',
        num_of_dpus=0,
        script_location='s3://datasci-poc-dag/dag/asset/rider_dispatch_shop_data_ingestion.py',
        iam_role_name='AWSGlueServiceRole-robinhood-live',
        s3_bucket='s3://datasci-poc-dag',
        retry_limit=0,
        create_job_kwargs={
            "GlueVersion": "3.0",
            "WorkerType": "G.1X",
            "NumberOfWorkers": 10,
            "DefaultArguments": {
                "--additional-python-modules": "sagemaker"
            }
        },
        script_args={
            "--busi_dt" : "{{ dag_run.conf['busi_dt'] }}"
		}
    )
	
	rider_usual_shop_weekly_ingestion_task = AwsGlueJobOperator(
        task_id="ingest-rider-usual-shop-data",
        job_name="rider-usual-shop-weekly-data-ingestion",
        job_desc="Glue job to ingest rider usual shop weekly feature into a feature store",
        region_name='ap-southeast-1',
        num_of_dpus=0,
        script_location='s3://datasci-poc-dag/dag/asset/rider_dispatch_rider_usual_shop_data_ingestion.py',
        iam_role_name='AWSGlueServiceRole-robinhood-live',
        s3_bucket='s3://datasci-poc-dag',
        retry_limit=0,
        create_job_kwargs={
            "GlueVersion": "3.0",
            "WorkerType": "G.1X",
            "NumberOfWorkers": 10,
            "DefaultArguments": {
                "--additional-python-modules": "sagemaker"
            }
        },
        script_args={
            "--busi_dt" : "{{ dag_run.conf['busi_dt'] }}"
		}
    )
	
	rider_usual_zone_weekly_ingestion_task = AwsGlueJobOperator(
        task_id="ingest-rider-usual-zone-data",
        job_name="rider-usual-zone-weekly-data-ingestion",
        job_desc="Glue job to ingest rider usual zone weekly feature into a feature store",
        region_name='ap-southeast-1',
        num_of_dpus=0,
        script_location='s3://datasci-poc-dag/dag/asset/rider_dispatch_rider_usual_zone_data_ingestion.py',
        iam_role_name='AWSGlueServiceRole-robinhood-live',
        s3_bucket='s3://datasci-poc-dag',
        retry_limit=0,
        create_job_kwargs={
            "GlueVersion": "3.0",
            "WorkerType": "G.1X",
            "NumberOfWorkers": 10,
            "DefaultArguments": {
                "--additional-python-modules": "sagemaker"
            }
        },
        script_args={
            "--busi_dt" : "{{ dag_run.conf['busi_dt'] }}"
		}
    )
	
	data_validation_task >> [rider_weekly_ingestion_task, shop_weekly_ingestion_task, rider_usual_shop_weekly_ingestion_task, rider_usual_zone_weekly_ingestion_task]