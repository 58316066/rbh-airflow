import botocore.session
import boto3
import json
from botocore.credentials import AssumeRoleCredentialFetcher, DeferredRefreshableCredentials
from airflow.hooks.base_hook import BaseHook
from botocore.exceptions import ClientError
from datetime import datetime , timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.decorators import dag, task
from airflow.providers.amazon.aws.operators.glue import AwsGlueJobOperator
from airflow.providers.amazon.aws.operators.glue_crawler import AwsGlueCrawlerOperator
# from airflow.providers.amazon.aws.hooks.glue_crawler import AWSGlueCrawlerOperator

from airflow.models import Variable
from appsflyer.default_api_pull_appsflyer import default_config , ios_oi_install , ios_noi_install , ios_oi_uninstall , ios_noi_uninstall
from airflow.utils.dates import days_ago

# Load Default Config
try:
    config = Variable.get("api_pull_appsflyer_config", default_config)
    config_ios_oi_install = Variable.get("config_ios_oi_install", ios_oi_install)
    config_ios_noi_install = Variable.get("config_ios_noi_install", ios_noi_install)
    config_ios_oi_uninstall = Variable.get("config_ios_oi_uninstall", ios_oi_uninstall)
    config_ios_noi_uninstall = Variable.get("config_ios_noi_uninstall", ios_noi_uninstall)
except:
    print('ERROR LOAD DEFULT CONFIG')

# Setup Params
# dag_run.conf['busi_dt']
# (datetime.now() ).strftime('%y%m%d')
AWS_CONN_ID = config.get('AWS_CONN_ID')
GLUE_SCRIPT_LOCN = config.get('GLUE_SCRIPT_LOCN')
S3_BUCKET = config.get('S3_BUCKET')
GLUEJOB_DESC = config.get('GLUEJOB_DESC')
IAM_ROLE_NAME = config.get('IAM_ROLE_NAME')
REGION_NAME = config.get('REGION_NAME')
NUM_OF_DPUS = config.get('NUM_OF_DPUS')
DAG_TAC = config.get('DAG_TAC')
RETRY_LIMIT = config.get('RETRY_LIMIT')
NUM_OF_WORKERS = config.get('NUM_OF_WORKERS')
GlueVersion = config.get('GlueVersion')
WorkerType = config.get('WorkerType')
GET_AGRS = config.get('GET_AGRS')
FIELD_PARTITION_S3 = config.get('FIELD_PARTITION_S3')
S3_OUTPUT_FILE  = config.get('S3_OUTPUT_FILE')
BUCKET_NAME = config.get('BUCKET_NAME')
PATH_NAME = config.get('PATH_NAME')

def get_boto3_session(assume_role_arn=None, region_name=None):
    session = boto3.Session(region_name=region_name)
    if not assume_role_arn:
        return session

    fetcher = AssumeRoleCredentialFetcher(
        client_creator=_get_client_creator(session),
        source_credentials=session.get_credentials(),
        role_arn=assume_role_arn,
    )
    botocore_session = botocore.session.Session()
    botocore_session._credentials = DeferredRefreshableCredentials(
        method="assume-role", refresh_using=fetcher.fetch_credentials
    )

    return boto3.Session(botocore_session=botocore_session, region_name=region_name)

def _get_client_creator(session):
    def client_creator(service_name, **kwargs):
        return session.client(service_name, **kwargs)

    return client_creator

    
# @dag(schedule_interval='3 * * * *', start_date=days_ago(1), catchup=False, tags=DAG_TAC)
@dag(schedule_interval=None, start_date=days_ago(1), catchup=False, tags=DAG_TAC)
def api_pull_appsflyer():
    ios_organic_installs_report = AwsGlueJobOperator(
        task_id=config_ios_oi_install.get('GLUEJOB_NAME'),
        job_name=config_ios_oi_install.get('GLUEJOB_NAME'),
        job_desc=GLUEJOB_DESC,
        region_name=REGION_NAME,
        num_of_dpus=NUM_OF_DPUS,
        script_location=GLUE_SCRIPT_LOCN,
        iam_role_name=IAM_ROLE_NAME,
        aws_conn_id=AWS_CONN_ID,
        s3_bucket=S3_BUCKET,
        retry_limit=RETRY_LIMIT,
        create_job_kwargs={
            "GlueVersion": GlueVersion,
            "WorkerType": WorkerType,
            "NumberOfWorkers": NUM_OF_WORKERS,
            "DefaultArguments": {
                "--EVENT_NAME": config_ios_oi_install.get('ARG_EVENT_NAME'),
                "--APP_ID":config_ios_oi_install.get('ARG_APP_ID'),
                "--EVENT_FROM_DT": config_ios_oi_install.get('ARG_EVENT_FROM_DT'),
                "--EVENT_TO_DT": config_ios_oi_install.get('ARG_EVENT_TO_DT'),
                "--API_TOKEN": config_ios_oi_install.get('ARG_API_TOKEN'),
                "--MAXIMUM_ROW": config_ios_oi_install.get('ARG_MAXIMUM_ROW'),
                "--TIMEZONE":config_ios_oi_install.get('ARG_TIMEZONE'),
                "--FIELDS": config_ios_oi_install.get('ARG_FIELDS'),
                "--FIELD_PARTITION_S3" : FIELD_PARTITION_S3,
                "--S3_OUTPUT_FILE" : S3_OUTPUT_FILE,
                "--BUCKET_NAME" : BUCKET_NAME,
                "--PATH_NAME" : PATH_NAME
            }
        }
    )

    ios_installs_report = AwsGlueJobOperator(
        task_id=config_ios_noi_install.get('GLUEJOB_NAME'),
        job_name=config_ios_noi_install.get('GLUEJOB_NAME'),
        job_desc=GLUEJOB_DESC,
        region_name=REGION_NAME,
        num_of_dpus=NUM_OF_DPUS,
        script_location=GLUE_SCRIPT_LOCN,
        iam_role_name=IAM_ROLE_NAME,
        aws_conn_id=AWS_CONN_ID,
        s3_bucket=S3_BUCKET,
        retry_limit=RETRY_LIMIT,
        create_job_kwargs={
            "GlueVersion": GlueVersion,
            "WorkerType": WorkerType,
            "NumberOfWorkers": NUM_OF_WORKERS,
            "DefaultArguments": {
                "--EVENT_NAME": config_ios_noi_install.get('ARG_EVENT_NAME'),
                "--APP_ID":config_ios_noi_install.get('ARG_APP_ID'),
                "--EVENT_FROM_DT": config_ios_noi_install.get('ARG_EVENT_FROM_DT'),
                "--EVENT_TO_DT": config_ios_noi_install.get('ARG_EVENT_TO_DT'),
                "--API_TOKEN": config_ios_noi_install.get('ARG_API_TOKEN'),
                "--MAXIMUM_ROW": config_ios_noi_install.get('ARG_MAXIMUM_ROW'),
                "--TIMEZONE":config_ios_noi_install.get('ARG_TIMEZONE'),
                "--FIELDS": config_ios_noi_install.get('ARG_FIELDS'),
                "--FIELD_PARTITION_S3" : FIELD_PARTITION_S3,
                "--S3_OUTPUT_FILE" : S3_OUTPUT_FILE,
                "--BUCKET_NAME" : BUCKET_NAME,
                "--PATH_NAME" : PATH_NAME
            }
        }
    )


    ios_organic_uninstall_report = AwsGlueJobOperator(
        task_id=config_ios_oi_uninstall.get('GLUEJOB_NAME'),
        job_name=config_ios_oi_uninstall.get('GLUEJOB_NAME'),
        job_desc=GLUEJOB_DESC,
        region_name=REGION_NAME,
        num_of_dpus=NUM_OF_DPUS,
        script_location=GLUE_SCRIPT_LOCN,
        iam_role_name=IAM_ROLE_NAME,
        aws_conn_id=AWS_CONN_ID,
        s3_bucket=S3_BUCKET,
        retry_limit=RETRY_LIMIT,
        create_job_kwargs={
            "GlueVersion": GlueVersion,
            "WorkerType": WorkerType,
            "NumberOfWorkers": NUM_OF_WORKERS,
            "DefaultArguments": {
                "--EVENT_NAME": config_ios_oi_uninstall.get('ARG_EVENT_NAME'),
                "--APP_ID":config_ios_oi_uninstall.get('ARG_APP_ID'),
                "--EVENT_FROM_DT": config_ios_oi_uninstall.get('ARG_EVENT_FROM_DT'),
                "--EVENT_TO_DT": config_ios_oi_uninstall.get('ARG_EVENT_TO_DT'),
                "--API_TOKEN": config_ios_oi_uninstall.get('ARG_API_TOKEN'),
                "--MAXIMUM_ROW": config_ios_oi_uninstall.get('ARG_MAXIMUM_ROW'),
                "--TIMEZONE":config_ios_oi_uninstall.get('ARG_TIMEZONE'),
                "--FIELDS": config_ios_oi_uninstall.get('ARG_FIELDS'),
                "--FIELD_PARTITION_S3" : FIELD_PARTITION_S3,
                "--S3_OUTPUT_FILE" : S3_OUTPUT_FILE,
                "--BUCKET_NAME" : BUCKET_NAME,
                "--PATH_NAME" : PATH_NAME
            }
        }
    )

    ios_uninstall_report = AwsGlueJobOperator(
        task_id=config_ios_noi_uninstall.get('GLUEJOB_NAME'),
        job_name=config_ios_noi_uninstall.get('GLUEJOB_NAME'),
        job_desc=GLUEJOB_DESC,
        region_name=REGION_NAME,
        num_of_dpus=NUM_OF_DPUS,
        script_location=GLUE_SCRIPT_LOCN,
        iam_role_name=IAM_ROLE_NAME,
        aws_conn_id=AWS_CONN_ID,
        s3_bucket=S3_BUCKET,
        retry_limit=RETRY_LIMIT,
        create_job_kwargs={
            "GlueVersion": GlueVersion,
            "WorkerType": WorkerType,
            "NumberOfWorkers": NUM_OF_WORKERS,
            "DefaultArguments": {
                "--EVENT_NAME": config_ios_noi_uninstall.get('ARG_EVENT_NAME'),
                "--APP_ID":config_ios_noi_uninstall.get('ARG_APP_ID'),
                "--EVENT_FROM_DT": config_ios_noi_uninstall.get('ARG_EVENT_FROM_DT'),
                "--EVENT_TO_DT": config_ios_noi_uninstall.get('ARG_EVENT_TO_DT'),
                "--API_TOKEN": config_ios_noi_uninstall.get('ARG_API_TOKEN'),
                "--MAXIMUM_ROW": config_ios_noi_uninstall.get('ARG_MAXIMUM_ROW'),
                "--TIMEZONE":config_ios_noi_uninstall.get('ARG_TIMEZONE'),
                "--FIELDS": config_ios_noi_uninstall.get('ARG_FIELDS'),
                "--FIELD_PARTITION_S3" : FIELD_PARTITION_S3,
                "--S3_OUTPUT_FILE" : S3_OUTPUT_FILE,
                "--BUCKET_NAME" : BUCKET_NAME,
                "--PATH_NAME" : PATH_NAME
            }
        }
    )



    @task
    def delete_a_glue_job(jobname: str, aws_conn_id: str):
        hook = BaseHook()
        extra = hook.get_connection(aws_conn_id).extra_dejson
        session = get_boto3_session(
            assume_role_arn=extra.get("role_arn"),
            region_name=extra.get("region_name"),
        )
        glue_client = session.client('glue')
        try:
            glue_client.delete_job(JobName=jobname)
            # return response
        except ClientError as e:
            raise Exception(
                "boto3 client error in delete_a_glue_job: " + e.__str__())
        except Exception as e:
            raise Exception(
                "Unexpected error in delete_a_glue_job: " + e.__str__())


    


    # Task assignment
    ios_organic_installs_report_delete = delete_a_glue_job(jobname=config_ios_oi_install.get('GLUEJOB_NAME'), aws_conn_id=AWS_CONN_ID)
    ios_installs_report_delete = delete_a_glue_job(jobname=config_ios_noi_install.get('GLUEJOB_NAME'), aws_conn_id=AWS_CONN_ID)
    ios_organic_uninstalls_report_delete = delete_a_glue_job(jobname=config_ios_oi_uninstall.get('GLUEJOB_NAME'), aws_conn_id=AWS_CONN_ID)
    ios_uninstalls_report_delete = delete_a_glue_job(jobname=config_ios_noi_uninstall.get('GLUEJOB_NAME'), aws_conn_id=AWS_CONN_ID)
    # load_conf_ios_oi_install = load_config_gluejob(config_ios_oi_install)
    # load_conf_ios_noi_install = load_config_gluejob(config_ios_noi_install)

    # delete_glue_job >> load_conf_ios_oi_install >> ios_organic_installs_report >> load_conf_ios_noi_install >> ios_installs_report

    ios_organic_installs_report_delete >> ios_installs_report_delete >> ios_organic_uninstalls_report_delete >> ios_uninstalls_report_delete >> [ios_organic_installs_report , ios_installs_report , ios_organic_uninstall_report , ios_uninstall_report] 
    # glue_call_api_task


api_pull_appsflyer = api_pull_appsflyer()