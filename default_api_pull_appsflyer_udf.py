default_config = {
    "AWS_CONN_ID" : 'aws_role_poc_glue_bq_connector_conn',
    "GLUEJOB_NAME" : "job_api_pull_appsflyer",
    "GLUE_SCRIPT_LOCN" : 's3://nat-glue-assets/appsflyer/api_pull_events_udf.py',
    "S3_BUCKET" : 's3://nat-mwaa-de-airflow-poc',
    "GLUEJOB_DESC" : "pull csv file from appsflyer using GET method dag pull_requert_daily",
    "IAM_ROLE_NAME" : 'poc-glue-bq-connector',
    "REGION_NAME" : 'ap-southeast-1',
    "NUM_OF_DPUS" : 0,
    "DAG_TAC" : ['poc'],
    "RETRY_LIMIT" : 0,
    "NUM_OF_WORKERS" : 2,
    "GlueVersion": "3.0",
    "WorkerType": "G.1X",
    "FIELD_PARTITION_S3" : "activity_dt_year, activity_dt_month, activity_dt_date",
    "S3_OUTPUT_FILE" : "s3://nat-fd-bronze/appsflyer/",
    "BUCKET_NAME" : "nat-fd-bronze",
    "PATH_NAME" : "appsflyer"
} 

ios_oi_install = {
    "GLUEJOB_NAME" : "job_ios_oi_install_udf",
    "ARG_EVENT_NAME" : 'organic_installs_report',
    "ARG_APP_ID" : 'id111180000',
    "ARG_EVENT_FROM_DT" : '2021-10-01',
    "ARG_EVENT_TO_DT" : '2021-10-31',
    "ARG_API_TOKEN" : '23f6fcfd-5ea2-420f-b2a2-8fd9f54dc789',
    "ARG_TIMEZONE" : "Asia%2fBangkok",
    "ARG_MAXIMUM_ROW" : "1000000",
    "ARG_FIELDS" : "device_model,keyword_id,store_reinstall,deeplink_url,oaid,install_app_store,gp_referrer,gp_click_time,gp_install_begin,amazon_aid,keyword_match_type,att,conversion_type,campaign_type,is_lat",
}

ios_noi_install = {
    "GLUEJOB_NAME" : "job_ios_noi_install_udf",
    "ARG_EVENT_NAME" : 'installs_report',
    "ARG_APP_ID" : 'id111180000',
    "ARG_EVENT_FROM_DT" : '2021-10-01',
    "ARG_EVENT_TO_DT" : '2021-10-31',
    "ARG_API_TOKEN" : '23f6fcfd-5ea2-420f-b2a2-8fd9f54dc789',
    "ARG_TIMEZONE" : "Asia%2fBangkok",
    "ARG_MAXIMUM_ROW" : "1000000",
    "ARG_FIELDS" : "device_model,keyword_id,store_reinstall,deeplink_url,oaid,install_app_store,gp_referrer,gp_click_time,gp_install_begin,amazon_aid,keyword_match_type,att,conversion_type,campaign_type,is_lat",
}

ios_oi_uninstall = {
    "GLUEJOB_NAME" : "job_ios_oi_uninstall_udf",
    "ARG_EVENT_NAME" : 'organic_uninstall_events_report',
    "ARG_APP_ID" : 'id111180000',
    "ARG_EVENT_FROM_DT" : '2021-10-01',
    "ARG_EVENT_TO_DT" : '2021-10-31',
    "ARG_API_TOKEN" : '23f6fcfd-5ea2-420f-b2a2-8fd9f54dc789',
    "ARG_TIMEZONE" : "Asia%2fBangkok",
    "ARG_MAXIMUM_ROW" : "1000000",
    "ARG_FIELDS" : "device_model,keyword_id,store_reinstall,deeplink_url,oaid,gp_referrer,gp_click_time,gp_install_begin,amazon_aid,keyword_match_type,is_lat",
}

ios_noi_uninstall = {
    "GLUEJOB_NAME" : "job_ios_noi_uninstall_udf",
    "ARG_EVENT_NAME" : 'uninstall_events_report',
    "ARG_APP_ID" : 'id111180000',
    "ARG_EVENT_FROM_DT" : '2021-10-01',
    "ARG_EVENT_TO_DT" : '2021-10-31',
    "ARG_API_TOKEN" : '23f6fcfd-5ea2-420f-b2a2-8fd9f54dc789',
    "ARG_TIMEZONE" : "Asia%2fBangkok",
    "ARG_MAXIMUM_ROW" : "1000000",
    "ARG_FIELDS" : "device_model,keyword_id,store_reinstall,deeplink_url,oaid,gp_referrer,gp_click_time,gp_install_begin,amazon_aid,keyword_match_type,is_lat",
}